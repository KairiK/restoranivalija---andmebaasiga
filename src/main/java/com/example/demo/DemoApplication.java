package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Config db tables");
		jdbcTemplate.execute("DROP TABLE IF EXISTS restod;");
		jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS restod(" +
				"nimi TEXT, " +
				"veeb TEXT, " +
				"aadress TEXT, " +
				"kaugusMin INTEGER, " +
				"tüüp TEXT, " +
				"hinnangToit INTEGER [], " +
				"ooteaeg INTEGER []);");

		jdbcTemplate.execute("INSERT INTO restod (nimi, veeb, aadress, kaugusMin, tüüp, hinnangToit, ooteaeg) " +
				"VALUES ('Washoku story', 'https://www.washoku-story.ee/', 'Poordi 3', 9, 'aasia', '{10}', '{12}');");
		jdbcTemplate.execute("INSERT INTO restod (nimi, veeb, aadress, kaugusMin, tüüp, hinnangToit, ooteaeg) " +
						"VALUES ('Chakra', 'http://www.chakra.ee/', 'Bremeni käik 1', 1, 'aasia', '{5}', '{20}');");
		//http://www.postgresqltutorial.com/postgresql-array/;
	}
}
