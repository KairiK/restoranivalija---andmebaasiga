#!/bin/sh

touch pid.file

kill $(cat pid.file)

nohup java -jar build/libs/demo-0.0.1-SNAPSHOT.jar > log.txt 2> errors.txt < /dev/null &

echo $! > pid.file
