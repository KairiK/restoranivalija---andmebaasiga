

package com.example.demo;

import ee.restovalija.Main;
import ee.restovalija.Resto;
import ee.restovalija.Tegevused;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class APIController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public APIController() {
    }

    @RequestMapping("/index")
    Resto index() {
        List<Map<String, Object>> koikRestod = jdbcTemplate.queryForList("SELECT nimi FROM restod;");
        ArrayList koikRestodArrayList = new ArrayList<>();
        for(Map e : koikRestod){
            koikRestodArrayList.add(e.get("nimi"));
        }
        String nimi = (String) koikRestodArrayList.get(Tegevused.suvalineArv(koikRestodArrayList));

        //tagastab DBst keskmise
        //WITH c AS (SELECT unnest(ooteaeg) as n from restod WHERE nimi = 'Washoku story') SELECT ROUND(AVG(n),0) FROM c;
        int hinnangtoit = (int) jdbcTemplate.queryForObject("WITH unnestitudTabel AS (SELECT unnest(hinnangtoit) as hinnanguVeerg from restod WHERE nimi = '"+ nimi +"') SELECT ROUND(AVG(hinnanguVeerg),0) FROM unnestitudTabel;", Integer.class);
        int ooteaeg = (int) jdbcTemplate.queryForObject("WITH unnestitudTabel AS (SELECT unnest(ooteaeg) as ooteajaVeerg from restod WHERE nimi = '"+ nimi +"') SELECT ROUND(AVG(ooteajaVeerg),0) FROM unnestitudTabel;", Integer.class);
        String koduleht = (String) jdbcTemplate.queryForObject("SELECT veeb FROM restod WHERE nimi = '" + nimi + "';", String.class);
        String aadress = (String) jdbcTemplate.queryForObject("SELECT aadress FROM restod WHERE nimi = '" + nimi + "';", String.class);
        String toidutyyp = (String) jdbcTemplate.queryForObject("SELECT tüüp FROM restod WHERE nimi = '" + nimi + "';", String.class);
        int kaugusMinutites = (int) jdbcTemplate.queryForObject("SELECT kaugusmin FROM restod WHERE nimi = '" + nimi + "';", Integer.class);

        Resto resto = new Resto(nimi, koduleht, aadress, kaugusMinutites, toidutyyp, hinnangtoit, ooteaeg);

        return resto;
    }

    @PostMapping("/uusresto")
    String uusResto(@RequestBody Resto resto) {
        jdbcTemplate.execute("INSERT INTO restod (nimi, veeb, aadress, kaugusmin, tüüp, hinnangtoit, ooteaeg) " +
                "VALUES ('" + resto.getNimi() + "', " +
                "'" +resto.getKoduleht() + "', " +
                "'" +resto.getAadress() + "', " +
                resto.getKaugusMinutites() + ", " +
                "'" +resto.getToidutyyp() + "', " +
                "'{" +resto.getHinnangToit() + "}', " +
                "'{" +resto.getOoteaeg() + "}');");
        return "Restoran andmebaasi lisatud";
    }

    @PostMapping("/hindaresto")
    String hindaResto(@RequestBody Resto resto) throws Exception {
        ArrayList numsHinnangtoit = jdbcTemplate.queryForObject("SELECT hinnangtoit FROM restod WHERE nimi = '" + resto.getNimi() + "';", ArrayList.class);
        ArrayList numsOoteaeg = jdbcTemplate.queryForObject("SELECT ooteaeg FROM restod WHERE nimi = '" + resto.getNimi() + "';", ArrayList.class);
        int hinnanguteArv = jdbcTemplate.queryForObject("SELECT array_length(hinnangtoit, 1) FROM restod WHERE nimi = '"+ resto.getNimi() +"';", Integer.class);
        int ooteajaArv = jdbcTemplate.queryForObject("SELECT array_length(ooteaeg, 1) FROM restod WHERE nimi = '"+ resto.getNimi() +"';", Integer.class);

        jdbcTemplate.execute("UPDATE restod SET hinnangtoit [" + ++hinnanguteArv + "] = '" + resto.getHinnangToit() + "' WHERE nimi = '" + resto.getNimi() + "';");
        jdbcTemplate.execute("UPDATE restod SET ooteaeg [" + ++ooteajaArv + "] = '" + resto.getOoteaeg() + "' WHERE nimi = '" + resto.getNimi() + "';");
        return "Hinnang restoranile antud";
    }

    @GetMapping("/kysiRestod")
    ArrayList<String> kysiRestod() {
        try {
            String sqlKask = "SELECT nimi FROM restod;";
            ArrayList<String> restod = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
                String resto = resultSet.getString("nimi");
                return new String(resto);
            });
            System.out.println(restod);
            return restod;
        } catch (DataAccessException err) {
            System.out.println("Table was not ready");
            return new ArrayList();
        }
    }
}
