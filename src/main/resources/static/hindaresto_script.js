console.log("konsool käivitus")

var kysiRestod = async function() {
	console.log("kysiRestod läks käima")
	var APIurl = url + '/kysiRestod'
	var request = await fetch(APIurl)
	var restod = await request.json()
    console.log(restod)
	while (restod.length > 0) {
		var resto = restod.pop()
		document.querySelector('#nimi').innerHTML +=
		     "<option value='" + resto + "'>" + resto + "</option>"
	}
}

kysiRestod()

document.querySelector('form').onsubmit = async function(event){
	event.preventDefault()
	// korjame kokku formist info
	console.log("submit käivitus")
	var nimi = document.querySelector("#nimi").value
	var hinnangToit = parseInt(document.querySelector("#hinnangToit").value)
	var ooteaeg = parseInt(document.querySelector("#ooteaeg").value)

    //tühjendame väljad ja ütleme aitäh
    document.querySelector('#hinnangToit').value = ""
    document.querySelector('#ooteaeg').value = ""
    document.querySelector("#koht").innerHTML = "Sisestatud. Aitäh!"
console.log(nimi, hinnangToit, ooteaeg)

	// POST päring postitab uue andmetüki serverisse
	var APIurl = url + "/hindaresto"
	await fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({nimi: nimi, hinnangToit: hinnangToit, ooteaeg: ooteaeg}),
		headers: {
			'Accept': 'application/json',
			'Content-Type':'application/json'

		}
	})
}